const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');

const JwtCoder = require('./JwtCoder');
const jwtCoder = new JwtCoder();

const PasswordCoder = require('./PasswordCoder');
const passwordCoder = new PasswordCoder();

const Persistence = require('./persistence');
const persistence = new Persistence();

router.all('/*', (req, res, next) => {
    const validate = () => {
        const token = req.header('authorization');
        if (token == undefined) {
            return res.status(401).json({error: "Authorization token not provided!"})
        }
        const decoded = jwtCoder.verify(token);
        if (!decoded) {
            return res.status(401).json({error: "Authorization failed, token not valid!"})
        }
        console.log("User successfully logged in: " + decoded.username);
        next();
    };
    const requiresAuthorizationHeader = (url) => {
        const notRequired = new Set(['/login']);
        return !notRequired.has(url);
    };

    if (requiresAuthorizationHeader(req.originalUrl)) {
        validate();
    } else {
        next();
    }
});

router.post('/login', [
    check('username').isEmail(),
    check('password').isLength({ min: 6 })
    ], (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) { return res.status(422).json({ errors: errors.array() }) }

    const username = req.body.username;
    const password = req.body.password;

    persistence.userExists(username).then((exists) => {
        if (!exists) { return res.status(404).json({error: "User does not exist!"}) }
        persistence.passwordForUser(username).then((hash) => {
            passwordCoder.compare(password, hash).then((matches) => {
                if (matches) {
                    const token = jwtCoder.sign(req.body.username);
                    res.json({token: token});
                } else {
                    res.status(401).json({error: "Password does not match!"});
                }
            });
        });
    });
});

module.exports = router;