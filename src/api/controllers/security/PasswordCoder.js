const bcrypt = require('bcrypt');

class PasswordCoder {
    constructor() {
        this.saltRounds = 10;
    }

    async encode(plainPassword) {
        return bcrypt.hash(plainPassword, this.saltRounds);
    }

    async compare(plainPassword, hashedPassword) {
        return bcrypt.compare(plainPassword, hashedPassword);
    }
}

module.exports = PasswordCoder;