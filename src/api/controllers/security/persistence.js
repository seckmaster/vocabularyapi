class Persistence {
    constructor() {
        this.database = {
            'test123@gmail.com': '$2b$10$swrOHRhW5JtrxJA25f3LfekSfLDUYAg/dZiPFjwPSqWeByzeuQfsu', // password: 'password'
            'rambo@john.de': '$2b$10$uk5ZnaSMUdrPTb8WwvHUeO2B.M68EyXMbHSGXUqxi2IPoz8euNcP6' // password: 'test123'
        }
    }

    async userExists(username) {
        return await this.database[username] != undefined;
    }

    async passwordForUser(username) {
        return await this.database[username];
    }
}

module.exports = Persistence;