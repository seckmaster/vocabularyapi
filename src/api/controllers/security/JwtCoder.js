const jwt = require('jsonwebtoken');

class JwtCoder {
    constructor(configuration) {
        this.expiresIn = "1h";
        this.algorithm = "HS256";
        this.secret = "xzpnpXtk8k1KmtHGuIUxmu6UMOT5gZ5V";

        if (!(configuration instanceof Object)) { return; }

        if (configuration.expiresIn != undefined) {
            this.expiresIn = configuration.expiresIn;
        }
        if (configuration.algorithm != undefined) {
            this.algorithm = configuration.algorithm;
        }
    }

    sign(username) {
        const token = jwt.sign({username: username}, this.secret, {algorithm: this.algorithm, expiresIn: this.expiresIn});
        return token;
    }

    verify(token) {
        return jwt.verify(token, this.secret, this.algorithm)
    }
}

module.exports = JwtCoder;