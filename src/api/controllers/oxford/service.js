const request = require("request");

class OxfordService {
    constructor() {
        this.appKey = '76d731d7b2b01d2c86cbf0c8eb3d7dbf';
        this.appId = '49204c4d';
        this.baseUrl = 'https://od-api.oxforddictionaries.com:443/api/v1/entries/en/';
    }

    translate(word, success, failure) {
        const url = this.apiUrl(word);
        const configuration = {
            headers: {
                'app_key': this.appKey,
                'app_id': this.appId
            }
        };
        request.get(url, configuration, (error, response, body) => {
            if (error != null) { return console.log("Error occurred!"); }
            response.statusCode == 200 ?
                this.handleSuccess(response, success) :
                this.handleFailure(response, failure);
        });
    }

    apiUrl(word) {
        return this.baseUrl + word;
    }

    handleSuccess(response, callback) {
        const convertedJson = this.map(JSON.parse(response.body).results[0]);
        callback(convertedJson);
    }

    map(json) {
        var definitions = [];
        json.lexicalEntries.forEach((lexicalEntry) => {
            lexicalEntry.entries.forEach((entry) => {
                var sense = entry.senses.map((sense) => {
                    const examples = sense.examples == undefined ?
                        [] :
                        sense.examples.map((example) => { return example.text });
                    return {"definition": {'description': sense.definitions, "examples": examples}};
                });
                if (!sense.empty) {
                    definitions.push(sense[0])
                }
            });
        });
        return {definitions};
    }

    handleFailure(response, callback) {
        callback(response.toJSON());
    }
}

module.exports = OxfordService;