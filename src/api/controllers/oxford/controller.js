const express = require('express');
const router = express.Router();
const OxfordService = require('./service');

const service = new OxfordService();

router.get('/oxford/translate/:word', (res, req, next) => {
    service.translate(res.params.word, (response) => {
        req.send(response);
    }, (json) => {
        req.send(json);
    });
});

module.exports = router;