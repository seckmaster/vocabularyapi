'use strict';

// Constants
const PORT = 8080;
const HOST = 'localhost';

// App
const express = require('express');
const app = express();

// Body parser
const bodyParser = require('body-parser');
app.use(bodyParser.json())

// Security
const authentication = require('./api/controllers/security/authentication');
app.use(authentication);

// Oxford
const oxfordController = require("./api/controllers/oxford/controller");
app.use(oxfordController);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);